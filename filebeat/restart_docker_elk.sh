#!/bin/sh
#SCRIPT RELANCER LE CONTENEUR PGLOGICALPG104

echo =======================================
echo ARRET DU CONTENEUR ELK
echo =======================================
#démarrage du container
sudo docker-compose -f /srv/scripts/protos/proto_elk/docker-compose.yml down

echo =======================================
echo DEMARRAGE DU CONTENEUR ELK
echo =======================================
sudo docker-compose -f /srv/scripts/protos/proto_elk/docker-compose.yml up -d

