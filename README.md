# Pile d'analyse de log ELK & Filebeat

- 1-Source

Site officiel des images docker pour la pile Elastic : https://www.docker.elastic.co/

Outils | Version | image docker 
- |:-: | -:
LOGSTASH | 6.3.1 sans XPack | docker pull docker.elastic.co/logstash/logstash-oss:6.3.1
ELASTIC SEARCH | 6.3.1 sans XPack | docker pull docker.elastic.co/elasticsearch/elasticsearch-oss:6.3.1
KIBANA | 6.3.1 sans XPack | docker pull docker.elastic.co/kibana/kibana-oss:6.3.1
FILEBEAT | 6.3.1 | docker pull docker.elastic.co/beats/filebeat:6.3.1


- 2-Schéma de l'architecture

- 3-Bon à savoir

- 4-Annexe
